import { Injectable } from "angular2/core";

@Injectable()
export class TimeService {
    serverDate = new Date();

    getDate() {
        return Promise.resolve(this.serverDate);
    }
}
