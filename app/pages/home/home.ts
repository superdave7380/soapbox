"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {WindowComponent} from "../../modules/window/window.component";
import {HeaderComponent} from "../../modules/header/header.component";
import {FooterComponent} from "../../modules/footer/footer.component";
import {BuyComponent} from "../../modules/buy/buy.component";

@Component({
    selector: "page-home",
    templateUrl: "pages/home/home.template.html",
    directives: [
        WindowComponent,
        HeaderComponent,
        FooterComponent,
        BuyComponent
    ]
})
export class Home {
    buyClicked: boolean;

    constructor() {
        console.log("Home component loaded");
    }
}
