import { Component, OnInit, EventEmitter, Output } from "angular2/core";
import { TimeService } from "../../core/services/time.service";

@Component({
    selector: "my-header",
    templateUrl: "../modules/header/header.template.html",
    providers: [TimeService]
})
export class HeaderComponent implements OnInit {
    currentDate: Date;

    constructor(private _timeService: TimeService) { }

    getCurrentTime() {
        this._timeService.getDate().then(currentDate => this.currentDate = currentDate);
    }

    startTimer() {
        setInterval(() => {
            this.currentDate = new Date(this.currentDate.setSeconds(this.currentDate.getSeconds()+1));
        }, 1000);
    }

    ngOnInit() {
        this.getCurrentTime();
        this.startTimer();
    }

    @Output() buy = new EventEmitter<boolean>();
}
