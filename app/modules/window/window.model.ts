export enum Type { TEXT, IMAGE }
// export enum State { InReservation, Reserved, Removed }
// export enum TZ { GMT-0, EST-5, PST-8 }
// export enum Country { Canada, USA }

export interface Thing {

    id: number;
    message?: string;
    image?: string;
    type: Type;
    url?: string;

    // state: State;
    // country: Country;
    // gmtTime: datetime;
    // tz: TZ;

}
