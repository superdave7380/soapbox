import { Component, OnInit } from "angular2/core";

import { Thing } from "./window.model";
import { Type } from "./window.model";
import { ThingService } from "./window.service";

@Component({
    selector: "my-window",
    templateUrl: "../modules/window/window.template.html",
    providers: [ThingService]
})
export class WindowComponent implements OnInit {
    thing: Thing;
    tempThing: Thing;
    thingType = Type;
    index: number;
    interval: number;

    constructor(private _thingService: ThingService) { }

    getThing(index: number) {
        this._thingService.getThing(index).then(thing => this.tempThing = thing);
    }

    ngOnInit() {
        this.index = 0;
        this.getThing(this.index);
        this.startTimer();
    }

    startTimer() {
        if (0 === this.index) {
            this.interval = setInterval(() => {
                this.getThing(++this.index);
                if (this.tempThing) {
                    this.thing = this.tempThing;
                } else {
                    clearInterval(this.interval);
                }
            }, 2000);
        }
    }
}
