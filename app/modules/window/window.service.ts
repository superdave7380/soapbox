import {THINGS} from "./mockThings";
import {Injectable} from "angular2/core";

@Injectable()
export class ThingService {
    getThing(index: number) {
        return Promise.resolve(THINGS[index]);
    }
}
