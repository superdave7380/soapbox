import { Thing } from "./window.model";
import { Type } from "./window.model";

export var THINGS: Thing[] = [
    {
        "id": 0,
        "message": "This is an",
        "type": Type.TEXT,
        "url": "#"
    },
    {
        "id": 1,
        "message": "Angular 2",
        "type": Type.TEXT,
        "url": "#"
    },
    {
        "id": 2,
        "message": "proof of concept.",
        "type": Type.TEXT,
        "url": "#"
    },
    {
        "id": 3,
        "message": "Bacon is delicious",
        "type": Type.TEXT,
        "url": "#"
    },
    {
        "id": 4,
        "image": "http://placehold.it/350x150",
        "type": Type.IMAGE,
        "url": "#"
    },
    {
        "id": 5,
        "image": "http://placehold.it/1100x620",
        "type": Type.IMAGE,
        "url": "#"
    },
    {
        "id": 6,
        "message": "Three cheers for bacon",
        "type": Type.TEXT,
        "url": "#"
    }
];
