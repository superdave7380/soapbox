import { Component, Input, Output, EventEmitter } from "angular2/core";

@Component({
    selector: "my-buy",
    templateUrl: "../modules/buy/buy.template.html",
    providers: []
})
export class BuyComponent {
    @Input() showMe: boolean;
    @Output() showMeChange = new EventEmitter();
}
